#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to generate extended csv (with boxes) from the kangaiset_annotation_short.csv file and the Manga109 folder

Usage: python generate_annotation_long.py --manga109_path PATH
"""

import numpy as np
from pathlib import Path
import os
import argparse
import pandas as pd
from tqdm import tqdm
import xml.etree.ElementTree as ET


#-------------------------------------------------------------------------------
__title__ =         "generate_annotation_long.py"
__copyright__ =     "University of La Rochelle, L3i/SAIL labs"
__credits__ =       ["Ruddy Theodose",]
__created__ =       "15/01/2024"
__license__ =       "CeCILL"
__version__ =       "0.1"
__maintainer__ =    "Ruddy Theodose"
__email__ =         "ruddy.theodose@univ-lr.fr"
#-------------------------------------------------------------------------------


"""

"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='generate_annotation_long.py',
        description='Create the long annotation file with the box coordinates',)
    parser.add_argument('--manga109_path', type=str, required=True)  # option that takes a value

    args = parser.parse_args()

    root_path = Path(args.manga109_path)
    images_folder = Path(root_path, "images")
    annotation_folder = Path(root_path, "annotations")
    list_mangas_dirs = [x for x in images_folder.glob("*") if x.is_dir()]
    print(len(list_mangas_dirs))
    print(list_mangas_dirs)
    print(list_mangas_dirs[0].name)

    list_data_all_mangas = []
    dict_faces = {}
    dict_bodies = {}
    dict_panels = {}

    cpt = 0
    # Each book has its own xml annotation file
    for manga_dir_path in list_mangas_dirs:
        annotation_path = Path(annotation_folder, manga_dir_path.name + ".xml")
        assert (annotation_path.exists())
        print(f"{cpt}/109 : manga_dir_path.name : {manga_dir_path.name}")

        # open xml annotation file
        tree = ET.parse(annotation_path)
        root = tree.getroot()

        characters_dict = {}
        list_faces_in_manga = []
        temp_character_ids = []

        for child in root:
            # print(child.tag, child.attrib)
            if child.tag == "characters":
                # on construit la base de personnages
                for character_node in child:
                    character_id = character_node.attrib["id"]
                    character_name = character_node.attrib["name"]
                    characters_dict[character_id] = character_name

            if child.tag == "pages":
                # on commence à parser les pages
                for page_node in child:
                    page_index = int(page_node.attrib["index"])
                    page_width = page_node.attrib["width"]
                    page_height = page_node.attrib["height"]
                    page_path = Path(manga_dir_path, f"{page_index:03}.jpg")
                    # print("page_path : ", page_path)
                    assert (page_path.exists() and page_path.is_file())

                    for content_node in page_node:
                        if content_node.tag == "face":
                            face_id = content_node.attrib["id"]
                            face_xmin = int(content_node.attrib["xmin"])
                            face_xmax = int(content_node.attrib["xmax"])
                            face_ymin = int(content_node.attrib["ymin"])
                            face_ymax = int(content_node.attrib["ymax"])
                            face_character_id = content_node.attrib["character"]

                            dict_faces[face_id]=[
                                face_id,
                                face_xmin, face_ymin, face_xmax, face_ymax,
                                face_character_id,
                            ]
                        if content_node.tag == "body":
                            body_id = content_node.attrib["id"]
                            body_xmin = int(content_node.attrib["xmin"])
                            body_xmax = int(content_node.attrib["xmax"])
                            body_ymin = int(content_node.attrib["ymin"])
                            body_ymax = int(content_node.attrib["ymax"])
                            body_character_id = content_node.attrib["character"]

                            dict_bodies[body_id]=[
                                body_id,
                                body_xmin, body_ymin, body_xmax, body_ymax,
                                body_character_id,
                            ]
                        if content_node.tag == "frame":
                            panel_id = content_node.attrib["id"]
                            panel_xmin = int(content_node.attrib["xmin"])
                            panel_xmax = int(content_node.attrib["xmax"])
                            panel_ymin = int(content_node.attrib["ymin"])
                            panel_ymax = int(content_node.attrib["ymax"])

                            dict_panels[panel_id]=[
                                panel_id,
                                panel_xmin, panel_ymin, panel_xmax, panel_ymax,
                            ]
        cpt+=1
    dataset_short_df = pd.read_csv("kangaiset_annotation_short.csv", index_col=0)
    dataset_long_df = dataset_short_df.copy()

    list_face_xmin = []
    list_face_ymin = []
    list_face_xmax = []
    list_face_ymax = []
    list_body_xmin = []
    list_body_ymin = []
    list_body_xmax = []
    list_body_ymax = []
    list_panel_xmin = []
    list_panel_ymin = []
    list_panel_xmax = []
    list_panel_ymax = []

    for i in tqdm(range(len(dataset_short_df.index))):
        row = dataset_short_df.iloc[i]
        face_id = row[f"face_id"]
        body_id = row[f"body_id"]
        panel_id = row[f"frame_id"]
        face_xmin = dict_faces[face_id][1]
        face_ymin = dict_faces[face_id][2]
        face_xmax = dict_faces[face_id][3]
        face_ymax = dict_faces[face_id][4]
        list_face_xmin.append(face_xmin)
        list_face_ymin.append(face_ymin)
        list_face_xmax.append(face_xmax)
        list_face_ymax.append(face_ymax)

        body_xmin = dict_bodies[body_id][1]
        body_ymin = dict_bodies[body_id][2]
        body_xmax = dict_bodies[body_id][3]
        body_ymax = dict_bodies[body_id][4]
        list_body_xmin.append(body_xmin)
        list_body_ymin.append(body_ymin)
        list_body_xmax.append(body_xmax)
        list_body_ymax.append(body_ymax)

        panel_xmin = dict_panels[panel_id][1]
        panel_ymin = dict_panels[panel_id][2]
        panel_xmax = dict_panels[panel_id][3]
        panel_ymax = dict_panels[panel_id][4]
        list_panel_xmin.append(panel_xmin)
        list_panel_ymin.append(panel_ymin)
        list_panel_xmax.append(panel_xmax)
        list_panel_ymax.append(panel_ymax)

    dataset_long_df["face_xmin"] = list_face_xmin
    dataset_long_df["face_ymin"] = list_face_ymin
    dataset_long_df["face_xmax"] = list_face_xmax
    dataset_long_df["face_ymax"] = list_face_ymax

    dataset_long_df["body_xmin"] = list_body_xmin
    dataset_long_df["body_ymin"] = list_body_ymin
    dataset_long_df["body_xmax"] = list_body_xmax
    dataset_long_df["body_ymax"] = list_body_ymax

    dataset_long_df["panel_xmin"] = list_panel_xmin
    dataset_long_df["panel_ymin"] = list_panel_ymin
    dataset_long_df["panel_xmax"] = list_panel_xmax
    dataset_long_df["panel_ymax"] = list_panel_ymax

    print("fin boucle")
    # mangas_faces_df = pd.DataFrame(dict_all_mangas)
    dataset_long_df.to_csv("kangaiset_annotation_long.csv", index=True)
