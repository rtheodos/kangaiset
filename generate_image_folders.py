#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script for displaying a row from kangaiset_annotation_long.csv

Usage: python generate_image_folders.py --manga109_path PATH --output_path OUTPUT_PATH
"""

import os
import pandas as pd
from pathlib import Path
import cv2
import numpy as np
import shutil
from tqdm import tqdm
import time
import argparse

#-------------------------------------------------------------------------------
__title__ =         "generate_image_folders.py"
__description__ =   "Script for generating image folders from kangaiset_annotation_long.csv"
__copyright__ =     "University of La Rochelle, L3i/SAIL labs"
__credits__ =       ["Ruddy Theodose",]
__created__ =       "15/01/2024"
__license__ =       "CeCILL"
__version__ =       "0.1"
__maintainer__ =    "Ruddy Theodose"
__email__ =         "ruddy.theodose@univ-lr.fr"
#-------------------------------------------------------------------------------

def rescale_box(
        xmin,
        ymin,
        xmax,
        ymax,
        scale,
        page_w,
        page_h,
):
    assert (xmax > xmin)
    assert (ymax > ymin)
    cur_w = xmax - xmin
    cur_h = ymax - ymin
    xc = (xmin + xmax) / 2.0
    yc = (ymin + ymax) / 2.0
    new_w = cur_w * scale
    new_h = cur_h * scale
    new_xmin = int(xc - new_w / 2.0)
    new_ymin = int(yc - new_h / 2.0)
    new_xmax = int(xc + new_w / 2.0)
    new_ymax = int(yc + new_h / 2.0)

    new_xmin = max(new_xmin, 0)
    new_ymin = max(new_ymin, 0)
    new_xmax = min(new_xmax, page_w - 1)
    new_ymax = min(new_ymax, page_h - 1)

    return new_xmin, new_ymin, new_xmax, new_ymax


def export_v3_separate_parts(
        manga109_folder,
        output_folder,
        test_books=None,
        scale_face=1.0,
        scale_body=1.0,
        scale_panel=1.0,
):
    csv_path = Path("kangaiset_annotation_long.csv")

    dataset_df = pd.read_csv(str(csv_path), index_col=0)
    set_mangas = sorted(set(dataset_df["manga_name"].tolist()))
    set_emotions = sorted(set(dataset_df["emotion_str"].tolist()))

    if "_" in set_emotions: set_emotions.remove("_")
    list_emotions = list(set_emotions)
    list_emotions = sorted([e for e in list_emotions if isinstance(e, str)])
    list_mangas = list(set_mangas)

    # output_global_folder = Path("/home/ruddy-theodose/Datasets/manga109_simple_20230408")

    output_face_folder = Path(output_folder, "face")
    output_body_folder = Path(output_folder, "body")
    output_panel_folder = Path(output_folder, "panel")

    for emotion_idx in range(len(list_emotions)):
        emotion = list_emotions[emotion_idx]
        print(f"{emotion}")
        emotion_df = dataset_df[dataset_df["emotion_str"] == emotion]

        output_face_all_folder = Path(output_face_folder, "all", emotion)
        output_face_train_folder = Path(output_face_folder, "train", emotion)
        output_face_test_folder = Path(output_face_folder, "test_external", emotion)

        output_body_all_folder = Path(output_body_folder, "all", emotion)
        output_body_train_folder = Path(output_body_folder, "train", emotion)
        output_body_test_folder = Path(output_body_folder, "test_external", emotion)

        output_panel_all_folder = Path(output_panel_folder, "all", emotion)
        output_panel_train_folder = Path(output_panel_folder, "train", emotion)
        output_panel_test_folder = Path(output_panel_folder, "test_external", emotion)

        output_face_all_folder.mkdir(parents=True, exist_ok=True)
        output_face_train_folder.mkdir(parents=True, exist_ok=True)
        output_face_test_folder.mkdir(parents=True, exist_ok=True)
        output_body_all_folder.mkdir(parents=True, exist_ok=True)
        output_body_train_folder.mkdir(parents=True, exist_ok=True)
        output_body_test_folder.mkdir(parents=True, exist_ok=True)
        output_panel_all_folder.mkdir(parents=True, exist_ok=True)
        output_panel_train_folder.mkdir(parents=True, exist_ok=True)
        output_panel_test_folder.mkdir(parents=True, exist_ok=True)

        for i in tqdm(range(len(emotion_df.index))):
            row = emotion_df.iloc[i]
            face_character_id = row["face_character_id"]
            face_id = row["face_id"]
            body_id = row["body_id"]
            panel_id = row["frame_id"]
            book_genre = row["book_genre"]
            manga_name = row["manga_name"]
            emotion_str = row["emotion_str"]
            page_path = Path(manga109_folder, row["page_path"])
            face_xmin_before = int(row["face_xmin"])
            face_xmax_before = int(row["face_xmax"])
            face_ymin_before = int(row["face_ymin"])
            face_ymax_before = int(row["face_ymax"])

            body_xmin_before = int(row["body_xmin"])
            body_xmax_before = int(row["body_xmax"])
            body_ymin_before = int(row["body_ymin"])
            body_ymax_before = int(row["body_ymax"])

            panel_xmin_before = int(row["panel_xmin"])
            panel_xmax_before = int(row["panel_xmax"])
            panel_ymin_before = int(row["panel_ymin"])
            panel_ymax_before = int(row["panel_ymax"])

            page_path = Path(manga109_folder, page_path)
            assert (page_path.exists())
            page_img = cv2.imread(str(page_path))
            page_height, page_width = page_img.shape[:2]

            face_xmin, face_ymin, face_xmax, face_ymax = rescale_box(
                face_xmin_before,
                face_ymin_before,
                face_xmax_before,
                face_ymax_before, scale_face,
                page_w=page_width, page_h=page_height
            )
            body_xmin, body_ymin, body_xmax, body_ymax = rescale_box(
                body_xmin_before,
                body_ymin_before,
                body_xmax_before,
                body_ymax_before, scale_body,
                page_w=page_width, page_h=page_height
            )
            panel_xmin, panel_ymin, panel_xmax, panel_ymax = rescale_box(
                panel_xmin_before,
                panel_ymin_before,
                panel_xmax_before,
                panel_ymax_before, scale_panel,
                page_w=page_width, page_h=page_height
            )

            face_img_patch = page_img[face_ymin:face_ymax, face_xmin:face_xmax]
            body_img_patch = page_img[body_ymin:body_ymax, body_xmin:body_xmax]
            panel_img_patch = page_img[panel_ymin:panel_ymax, panel_xmin:panel_xmax]

            if test_books is not None and manga_name in test_books:
                cur_output_face_path = Path(
                    output_face_test_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}.png",
                )
                cur_output_body_path = Path(
                    output_body_test_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{body_id}.png",
                )
                cur_output_panel_path = Path(
                    output_panel_test_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{panel_id}.png",
                )
            else:
                cur_output_face_path = Path(
                    output_face_train_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}.png",
                )
                cur_output_body_path = Path(
                    output_body_train_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{body_id}.png",
                )
                cur_output_panel_path = Path(
                    output_panel_train_folder,
                    f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{panel_id}.png",
                )
            cv2.imwrite(str(cur_output_face_path), face_img_patch)
            cv2.imwrite(str(cur_output_body_path), body_img_patch)
            cv2.imwrite(str(cur_output_panel_path), panel_img_patch)

            cur_output_face_path = Path(
                output_face_all_folder,
                f"{book_genre}_{manga_name}_{face_character_id}_{face_id}.png",
            )
            cur_output_body_path = Path(
                output_body_all_folder,
                f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{body_id}.png",
            )
            cur_output_panel_path = Path(
                output_panel_all_folder,
                f"{book_genre}_{manga_name}_{face_character_id}_{face_id}_{panel_id}.png",
            )
            cv2.imwrite(str(cur_output_face_path), face_img_patch)
            cv2.imwrite(str(cur_output_body_path), body_img_patch)
            cv2.imwrite(str(cur_output_panel_path), panel_img_patch)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='generate_image_folders.py',
        description='Create the folders containing the extracted patches from the three types of content')
    parser.add_argument('--manga109_folder', type=str, required=True, help="Location of Manga109 dataset")
    parser.add_argument('--output_folder', type=str, required=True, help="Location of output folder")
    parser.add_argument('--scale_face', type=float, default=1.0, help="Scale for face patches")
    parser.add_argument('--scale_body', type=float, default=1.0, help="Scale for body patches")
    parser.add_argument('--scale_panel', type=float, default=1.0, help="Scale for panel patches")
    args = parser.parse_args()

    # List test books
    test_books = [
        "EvaLady",  # science fiction
        "Donburakokko",  # romantic comedy
        "Count3DeKimeteAgeru",  # sport
        "TapkunNoTanteisitsu",  # animal
        "MutekiBoukenSyakuma",  # battle
        "NichijouSoup",  # humor
    ]
    t0 = time.time()
    export_v3_separate_parts(
        manga109_folder=args.manga109_folder,
        output_folder=args.output_folder,
        test_books=test_books,
        scale_face=args.scale_face,
        scale_body=args.scale_body,
        scale_panel=args.scale_panel,
    )  # environ 10min
    t1 = time.time()
    print(f"{t1 - t0}s")
