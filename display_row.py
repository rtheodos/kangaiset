#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script for displaying a row from kangaiset_annotation_long.csv

Usage: python display_row.py --manga109_path PATH
"""

import numpy as np
from pathlib import Path
import os
import argparse
import pandas as pd
from tqdm import tqdm
import xml.etree.ElementTree as ET
import cv2
import matplotlib.pyplot as plt

#-------------------------------------------------------------------------------
__title__ =         "display_row.py"
__copyright__ =     "University of La Rochelle, L3i/SAIL labs"
__credits__ =       ["Ruddy Theodose",]
__created__ =       "15/01/2024"
__license__ =       "CeCILL"
__version__ =       "0.1"
__maintainer__ =    "Ruddy Theodose"
__email__ =         "ruddy.theodose@univ-lr.fr"
#-------------------------------------------------------------------------------


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='generate_annotation_long.py',
        description='Create the long annotation file with the box coordinates', )
    parser.add_argument('--manga109_path', type=str, required=True)  # option that takes a value
    args = parser.parse_args()

    root_path = Path(args.manga109_path)
    dataset_df = pd.read_csv("kangaiset_annotation_long.csv", index_col=0)

    # Select row and extract infos
    i = np.random.randint(0, len(dataset_df.index))
    row = dataset_df.iloc[i]
    face_id = row[f"face_id"]
    body_id = row[f"body_id"]
    panel_id = row[f"frame_id"]
    page_path = os.path.join(root_path, row["page_path"])
    face_xmin = int(row[f"face_xmin"])
    face_ymin = int(row[f"face_ymin"])
    face_xmax = int(row[f"face_xmax"])
    face_ymax = int(row[f"face_ymax"])
    body_xmin = int(row[f"body_xmin"])
    body_ymin = int(row[f"body_ymin"])
    body_xmax = int(row[f"body_xmax"])
    body_ymax = int(row[f"body_ymax"])
    panel_xmin = int(row[f"panel_xmin"])
    panel_ymin = int(row[f"panel_ymin"])
    panel_xmax = int(row[f"panel_xmax"])
    panel_ymax = int(row[f"panel_ymax"])
    face_xs = [face_xmin, face_xmin, face_xmax, face_xmax, face_xmin]
    face_ys = [face_ymin, face_ymax, face_ymax, face_ymin, face_ymin]
    body_xs = [body_xmin, body_xmin, body_xmax, body_xmax, body_xmin]
    body_ys = [body_ymin, body_ymax, body_ymax, body_ymin, body_ymin]
    panel_xs = [panel_xmin, panel_xmin, panel_xmax, panel_xmax, panel_xmin]
    panel_ys = [panel_ymin, panel_ymax, panel_ymax, panel_ymin, panel_ymin]

    # load page
    img = cv2.imread(page_path)
    img = img[:, :, ::-1]

    # display page with boxes
    plt.figure()
    plt.imshow(img)
    plt.plot(panel_xs, panel_ys, color="b")
    plt.plot(body_xs, body_ys, color="g")
    plt.plot(face_xs, face_ys, color="r")
    plt.show()
