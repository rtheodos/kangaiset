hello there
# KangaiSet : A dataset for visual emotion recognition on Manga

This repository aims at provinding tools to use our dataset for visual emotion recognition on mangas.

As manga pages and box annotations does not belong to us, **_this dataset cannot be used without the Manga109 dataset_**

In this repository, we consider two annotations files:
- kangaiset_annotations_short.csv (included in the repository) : contains the references of the boxes associated to our dataset;
- kangaiset_annotations_long.csv (must be generated) : contains all the boxes, the coordinates of the points... for all the boxes in the Manga109 dataset.

## Installation
Run `pip install -r requirements.txt` to install the corresponding packages.

## Generation of annotations_long.csv
To generate the annotation file, run `python generate_annotation_long.py --manga109_path MANGA109_PATH`
The annotation file will be created in the current folder.

## Generation of image folders
To generate image folder : run
`python generate_image_folders.py --manga109_folder MANGA109_PATH --output_folder OUTPUT_PATH `

Image folders are organized with the tree structure
```
|----main_folder
|    |----face
|    |    |----all
|    |    |     |----anger
|    |    |     |     |----img_0.png
|    |    |     |     |----img_1.png
|    |    |     |     ...
|    |    |     |----disgust
|    |    |     |----fear
|    |    |     ...
|    |----body
|        |----all
|        |    |     |----anger
|        |    |     |     |----img_0.png
|        |    |     |     |----img_1.png
|    |----panel
|        |----body
|        |----all
|        |    |     |----anger
|        |    |     |     |----img_0.png
|        |    |     |     |----img_1.png
```

Each "all" folder is compatible with the pytorch "ImageFolder" class


